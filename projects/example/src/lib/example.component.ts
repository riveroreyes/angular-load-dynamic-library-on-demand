import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'riveroreyes-example',
  template: `
    <p>
      example works!
    </p>
  `,
  styles: [
  ]
})
export class ExampleComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
