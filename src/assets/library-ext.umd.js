(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/router')) :
    typeof define === 'function' && define.amd ? define('library-ext', ['exports', '@angular/core', '@angular/router'], factory) :
    (global = global || self, factory(global['library-ext'] = {}, global.ng.core, global.ng.router));
}(this, (function (exports, core, router) { 'use strict';

    var LibraryExtService = /** @class */ (function () {
        function LibraryExtService() {
        }
        LibraryExtService.ɵfac = function LibraryExtService_Factory(t) { return new (t || LibraryExtService)(); };
        LibraryExtService.ɵprov = core.ɵɵdefineInjectable({ token: LibraryExtService, factory: LibraryExtService.ɵfac, providedIn: 'root' });
        return LibraryExtService;
    }());
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(LibraryExtService, [{
            type: core.Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], function () { return []; }, null); })();

    var LibraryExtComponent = /** @class */ (function () {
        function LibraryExtComponent() {
        }
        LibraryExtComponent.prototype.ngOnInit = function () {
        };
        LibraryExtComponent.ɵfac = function LibraryExtComponent_Factory(t) { return new (t || LibraryExtComponent)(); };
        LibraryExtComponent.ɵcmp = core.ɵɵdefineComponent({ type: LibraryExtComponent, selectors: [["disashop-library-ext"]], decls: 2, vars: 0, template: function LibraryExtComponent_Template(rf, ctx) { if (rf & 1) {
                core.ɵɵelementStart(0, "p");
                core.ɵɵtext(1, " library-ext works! ");
                core.ɵɵelementEnd();
            } }, encapsulation: 2 });
        return LibraryExtComponent;
    }());
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(LibraryExtComponent, [{
            type: core.Component,
            args: [{
                    selector: 'disashop-library-ext',
                    template: "\n    <p>\n      library-ext works!\n    </p>\n  ",
                    styles: []
                }]
        }], function () { return []; }, null); })();

    var LibraryExtContentComponent = /** @class */ (function () {
        function LibraryExtContentComponent() {
        }
        LibraryExtContentComponent.prototype.ngOnInit = function () {
        };
        LibraryExtContentComponent.ɵfac = function LibraryExtContentComponent_Factory(t) { return new (t || LibraryExtContentComponent)(); };
        LibraryExtContentComponent.ɵcmp = core.ɵɵdefineComponent({ type: LibraryExtContentComponent, selectors: [["disashop-library-ext-content"]], decls: 11, vars: 0, consts: [[1, "container"], [1, "card"], [1, "card-header"], [1, "card-body"], [1, "card-title"], [1, "card-text"], ["href", "#", 1, "btn", "btn-primary"]], template: function LibraryExtContentComponent_Template(rf, ctx) { if (rf & 1) {
                core.ɵɵelementStart(0, "div", 0);
                core.ɵɵelementStart(1, "div", 1);
                core.ɵɵelementStart(2, "div", 2);
                core.ɵɵtext(3, " Featured ");
                core.ɵɵelementEnd();
                core.ɵɵelementStart(4, "div", 3);
                core.ɵɵelementStart(5, "h5", 4);
                core.ɵɵtext(6, "Special title treatment");
                core.ɵɵelementEnd();
                core.ɵɵelementStart(7, "p", 5);
                core.ɵɵtext(8, "With supporting text below as a natural lead-in to additional content.");
                core.ɵɵelementEnd();
                core.ɵɵelementStart(9, "a", 6);
                core.ɵɵtext(10, "Go somewhere");
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
            } }, styles: [""] });
        return LibraryExtContentComponent;
    }());
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(LibraryExtContentComponent, [{
            type: core.Component,
            args: [{
                    selector: 'disashop-library-ext-content',
                    templateUrl: './library-ext-content.component.html',
                    styleUrls: ['./library-ext-content.component.css']
                }]
        }], function () { return []; }, null); })();

    var routes = [
        { path: 'content', component: LibraryExtContentComponent },
        { path: '', redirectTo: '', pathMatch: 'full' },
    ];
    var LibraryExtModule = /** @class */ (function () {
        function LibraryExtModule() {
        }
        LibraryExtModule.ɵmod = core.ɵɵdefineNgModule({ type: LibraryExtModule });
        LibraryExtModule.ɵinj = core.ɵɵdefineInjector({ factory: function LibraryExtModule_Factory(t) { return new (t || LibraryExtModule)(); }, imports: [[
                    router.RouterModule.forChild(routes)
                ],
                router.RouterModule] });
        return LibraryExtModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core.ɵɵsetNgModuleScope(LibraryExtModule, { declarations: [LibraryExtComponent, LibraryExtContentComponent], imports: [router.RouterModule], exports: [LibraryExtComponent, router.RouterModule] }); })();
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(LibraryExtModule, [{
            type: core.NgModule,
            args: [{
                    declarations: [LibraryExtComponent, LibraryExtContentComponent],
                    imports: [
                        router.RouterModule.forChild(routes)
                    ],
                    exports: [LibraryExtComponent, router.RouterModule]
                }]
        }], null, null); })();

    exports.LibraryExtComponent = LibraryExtComponent;
    exports.LibraryExtModule = LibraryExtModule;
    exports.LibraryExtService = LibraryExtService;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=library-ext.umd.js.map
