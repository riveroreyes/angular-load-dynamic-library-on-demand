(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('ddr-click-outside'), require('@angular/forms'), require('@angular/common')) :
    typeof define === 'function' && define.amd ? define('@riveroreyes/optare-dropdown', ['exports', '@angular/core', 'ddr-click-outside', '@angular/forms', '@angular/common'], factory) :
    (global = global || self, factory((global.riveroreyes = global.riveroreyes || {}, global.riveroreyes['optare-dropdown'] = {}), global.ng.core, global.DdrClickOutsideModule, global.ng.forms, global.ng.common));
}(this, (function (exports, core, ddrClickOutside, forms, common) { 'use strict';

    var OptareDropdownService = /** @class */ (function () {
        function OptareDropdownService() {
            this.optionsShow = [];
            this.selecteEmitter = new core.EventEmitter();
        }
        OptareDropdownService.prototype.setOptions = function (options) {
            this.options = options.slice();
            this.optionsShow = this.options.slice();
        };
        OptareDropdownService.prototype.setValueSelected = function (value) {
            this.valueSelected = value;
            this.preload();
        };
        OptareDropdownService.prototype.preload = function () {
            var _this = this;
            var optionsFound = null;
            if (typeof this.valueSelected === 'object') {
                optionsFound = this.options.find(function (option) { return JSON.stringify(option.value) === JSON.stringify(_this.valueSelected); });
            }
            else {
                optionsFound = this.options.find(function (option) { return option.value === _this.valueSelected; });
            }
            if (optionsFound) {
                this.valueShow = optionsFound.label;
                this.selectItem(optionsFound);
            }
        };
        OptareDropdownService.prototype.selectItem = function (item) {
            this.showItems = false;
            this.valueShow = item.label;
            this.selecteEmitter.emit(item);
            this.optionsShow = this.options.slice();
        };
        OptareDropdownService.ɵfac = function OptareDropdownService_Factory(t) { return new (t || OptareDropdownService)(); };
        OptareDropdownService.ɵprov = core.ɵɵdefineInjectable({ token: OptareDropdownService, factory: OptareDropdownService.ɵfac, providedIn: 'root' });
        return OptareDropdownService;
    }());
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(OptareDropdownService, [{
            type: core.Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], function () { return []; }, null); })();

    function OptareDropdownComponent_div_6_div_7_Template(rf, ctx) { if (rf & 1) {
        core.ɵɵelementStart(0, "div", 13);
        core.ɵɵelementStart(1, "span");
        core.ɵɵtext(2);
        core.ɵɵelementEnd();
        core.ɵɵelementEnd();
    } if (rf & 2) {
        var ctx_r2 = core.ɵɵnextContext(2);
        core.ɵɵadvance(2);
        core.ɵɵtextInterpolate(ctx_r2.labelNoResults);
    } }
    function OptareDropdownComponent_div_6_ul_8_li_1_Template(rf, ctx) { if (rf & 1) {
        var _r7 = core.ɵɵgetCurrentView();
        core.ɵɵelementStart(0, "li", 15);
        core.ɵɵlistener("click", function OptareDropdownComponent_div_6_ul_8_li_1_Template_li_click_0_listener() { core.ɵɵrestoreView(_r7); var item_r5 = ctx.$implicit; var ctx_r6 = core.ɵɵnextContext(3); return ctx_r6.opService.selectItem(item_r5); });
        core.ɵɵtext(1);
        core.ɵɵelementEnd();
    } if (rf & 2) {
        var item_r5 = ctx.$implicit;
        core.ɵɵadvance(1);
        core.ɵɵtextInterpolate1(" ", item_r5.label, " ");
    } }
    function OptareDropdownComponent_div_6_ul_8_Template(rf, ctx) { if (rf & 1) {
        core.ɵɵelementStart(0, "ul");
        core.ɵɵtemplate(1, OptareDropdownComponent_div_6_ul_8_li_1_Template, 2, 1, "li", 14);
        core.ɵɵelementEnd();
    } if (rf & 2) {
        var ctx_r3 = core.ɵɵnextContext(2);
        core.ɵɵadvance(1);
        core.ɵɵproperty("ngForOf", ctx_r3.opService.optionsShow);
    } }
    function OptareDropdownComponent_div_6_Template(rf, ctx) { if (rf & 1) {
        var _r9 = core.ɵɵgetCurrentView();
        core.ɵɵelementStart(0, "div", 6);
        core.ɵɵelementStart(1, "div", 7);
        core.ɵɵelementStart(2, "input", 8, 9);
        core.ɵɵlistener("keyup", function OptareDropdownComponent_div_6_Template_input_keyup_2_listener() { core.ɵɵrestoreView(_r9); var _r1 = core.ɵɵreference(3); var ctx_r8 = core.ɵɵnextContext(); return ctx_r8.filter(_r1.value); });
        core.ɵɵelementEnd();
        core.ɵɵelementStart(4, "div", 3);
        core.ɵɵelementStart(5, "button");
        core.ɵɵelement(6, "i", 10);
        core.ɵɵelementEnd();
        core.ɵɵelementEnd();
        core.ɵɵelementEnd();
        core.ɵɵtemplate(7, OptareDropdownComponent_div_6_div_7_Template, 3, 1, "div", 11);
        core.ɵɵtemplate(8, OptareDropdownComponent_div_6_ul_8_Template, 2, 1, "ul", 12);
        core.ɵɵelementEnd();
    } if (rf & 2) {
        var ctx_r0 = core.ɵɵnextContext();
        core.ɵɵadvance(7);
        core.ɵɵproperty("ngIf", ctx_r0.opService.optionsShow.length === 0);
        core.ɵɵadvance(1);
        core.ɵɵproperty("ngIf", ctx_r0.opService.optionsShow.length > 0);
    } }
    var OptareDropdownComponent = /** @class */ (function () {
        function OptareDropdownComponent(opService) {
            this.opService = opService;
            this.labelNoResults = 'No se encontraron resultados';
            this.selecteEmitter = this.opService.selecteEmitter;
            this.opService.showItems = false;
        }
        OptareDropdownComponent.prototype.ngOnInit = function () {
            this.opService.options = this.options;
            this.opService.valueSelected = this.valueSelected;
            if (this.opService.valueSelected) {
                this.opService.preload();
            }
            this.opService.optionsShow = this.options.slice();
        };
        OptareDropdownComponent.prototype.showPanelOptions = function () {
            this.opService.showItems = !this.opService.showItems;
        };
        OptareDropdownComponent.prototype.filter = function (value) {
            this.opService.optionsShow = this.options.filter(function (x) { return x.label.toLowerCase().includes(value.toLowerCase()); });
        };
        OptareDropdownComponent.prototype.clickOutside = function ($event) {
            this.opService.showItems = false;
            this.opService.optionsShow = this.options.slice();
        };
        OptareDropdownComponent.ɵfac = function OptareDropdownComponent_Factory(t) { return new (t || OptareDropdownComponent)(core.ɵɵdirectiveInject(OptareDropdownService)); };
        OptareDropdownComponent.ɵcmp = core.ɵɵdefineComponent({ type: OptareDropdownComponent, selectors: [["disashop-optare-dropdown"]], inputs: { options: "options", valueSelected: "valueSelected", labelNoResults: "labelNoResults" }, outputs: { selecteEmitter: "selecteEmitter" }, decls: 7, vars: 2, consts: [["ddrDdrClickOutside", "", 1, "ddr-dropdown", 3, "clickOutside"], [1, "input-group", 3, "click"], ["type", "text", "name", "value", "readonly", "", 1, "form-control", 3, "ngModel", "ngModelChange"], [1, "input-group-append"], ["aria-hidden", "true", 1, "fa", "fa-caret-down"], ["class", "panel-items", 4, "ngIf"], [1, "panel-items"], [1, "search", "input-group"], ["type", "text", "name", "value", 1, "form-control", 3, "keyup"], ["search", ""], ["aria-hidden", "true", 1, "fa", "fa-search"], ["class", "no-results", 4, "ngIf"], [4, "ngIf"], [1, "no-results"], [3, "click", 4, "ngFor", "ngForOf"], [3, "click"]], template: function OptareDropdownComponent_Template(rf, ctx) { if (rf & 1) {
                core.ɵɵelementStart(0, "div", 0);
                core.ɵɵlistener("clickOutside", function OptareDropdownComponent_Template_div_clickOutside_0_listener($event) { return ctx.clickOutside($event); });
                core.ɵɵelementStart(1, "div", 1);
                core.ɵɵlistener("click", function OptareDropdownComponent_Template_div_click_1_listener() { return ctx.showPanelOptions(); });
                core.ɵɵelementStart(2, "input", 2);
                core.ɵɵlistener("ngModelChange", function OptareDropdownComponent_Template_input_ngModelChange_2_listener($event) { return ctx.opService.valueShow = $event; });
                core.ɵɵelementEnd();
                core.ɵɵelementStart(3, "div", 3);
                core.ɵɵelementStart(4, "button");
                core.ɵɵelement(5, "i", 4);
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵtemplate(6, OptareDropdownComponent_div_6_Template, 9, 2, "div", 5);
                core.ɵɵelementEnd();
            } if (rf & 2) {
                core.ɵɵadvance(2);
                core.ɵɵproperty("ngModel", ctx.opService.valueShow);
                core.ɵɵadvance(4);
                core.ɵɵproperty("ngIf", ctx.opService.showItems);
            } }, directives: [ddrClickOutside.DdrClickOutsideDirective, forms.DefaultValueAccessor, forms.NgControlStatus, forms.NgModel, common.NgIf, common.NgForOf], styles: ["disashop-optare-dropdown .ddr-dropdown{min-width:100px;position:relative}disashop-optare-dropdown .form-control[readonly]{background:#fff;cursor:pointer}disashop-optare-dropdown input:focus{outline:0!important;-webkit-appearance:none;box-shadow:none!important}disashop-optare-dropdown .ddr-dropdown button{height:100%!important;width:32px;background-color:transparent;border-color:transparent;position:absolute;right:0;z-index:999}disashop-optare-dropdown .panel-items{position:absolute;width:100%;background-color:#fff;z-index:999}disashop-optare-dropdown .panel-items ul{margin:0;padding:0;max-height:200px;overflow:auto;border:1px solid #c8c8c8;border-top:0}disashop-optare-dropdown .panel-items ul li{list-style:none;cursor:pointer;padding:5px}disashop-optare-dropdown .panel-items ul li:hover{background:#aefff48c}disashop-optare-dropdown .no-results{padding:5px;border:1px solid #c8c8c8;font-size:14px;font-style:italic}disashop-optare-dropdown .ddr-dropdown input{border-radius:0;outline:0}disashop-optare-dropdown .input-group-append{margin-left:0}"], encapsulation: 2 });
        return OptareDropdownComponent;
    }());
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(OptareDropdownComponent, [{
            type: core.Component,
            args: [{
                    selector: 'disashop-optare-dropdown',
                    templateUrl: './optare-dropdown.component.html',
                    styleUrls: ['./optare-dropdown.component.css'],
                    encapsulation: core.ViewEncapsulation.None
                }]
        }], function () { return [{ type: OptareDropdownService }]; }, { options: [{
                type: core.Input
            }], valueSelected: [{
                type: core.Input
            }], labelNoResults: [{
                type: core.Input
            }], selecteEmitter: [{
                type: core.Output
            }] }); })();

    var OptareSelectItem = /** @class */ (function () {
        function OptareSelectItem(label, value) {
            this.label = label;
            this.value = value;
        }
        return OptareSelectItem;
    }());

    var OptareDropdownModule = /** @class */ (function () {
        function OptareDropdownModule() {
        }
        OptareDropdownModule.ɵmod = core.ɵɵdefineNgModule({ type: OptareDropdownModule });
        OptareDropdownModule.ɵinj = core.ɵɵdefineInjector({ factory: function OptareDropdownModule_Factory(t) { return new (t || OptareDropdownModule)(); }, imports: [[
                    common.CommonModule, forms.FormsModule, ddrClickOutside.DdrClickOutsideModule
                ]] });
        return OptareDropdownModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core.ɵɵsetNgModuleScope(OptareDropdownModule, { declarations: [OptareDropdownComponent], imports: [common.CommonModule, forms.FormsModule, ddrClickOutside.DdrClickOutsideModule], exports: [OptareDropdownComponent] }); })();
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(OptareDropdownModule, [{
            type: core.NgModule,
            args: [{
                    declarations: [OptareDropdownComponent],
                    imports: [
                        common.CommonModule, forms.FormsModule, ddrClickOutside.DdrClickOutsideModule
                    ],
                    exports: [OptareDropdownComponent],
                    schemas: [core.CUSTOM_ELEMENTS_SCHEMA]
                }]
        }], null, null); })();

    exports.OptareDropdownComponent = OptareDropdownComponent;
    exports.OptareDropdownModule = OptareDropdownModule;
    exports.OptareDropdownService = OptareDropdownService;
    exports.OptareSelectItem = OptareSelectItem;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=riveroreyes-optare-dropdown.umd.js.map
