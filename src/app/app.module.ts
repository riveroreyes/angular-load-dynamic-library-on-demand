import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CompilerFactory, COMPILER_OPTIONS, Compiler, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ClarityModule, CUSTOM_BUTTON_TYPES } from '@clr/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RouterService } from './services/router.service';
import { ModuleService } from './services/module.service';
import { JitCompilerFactory } from '@angular/platform-browser-dynamic';
import { OtraPaginaComponent } from './otra-pagina/otra-pagina.component';

export function createCompiler(compilerFactory: CompilerFactory) {
  return compilerFactory.createCompiler();
}

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    OtraPaginaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ClarityModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [RouterService, ModuleService,
    { provide: COMPILER_OPTIONS, useValue: {}, multi: true },
    { provide: CompilerFactory, useClass: JitCompilerFactory, deps: [COMPILER_OPTIONS] },
    { provide: Compiler, useFactory: createCompiler, deps: [CompilerFactory] }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
